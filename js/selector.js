
var elmFocus = null;
var typeXpath = 'text';
var container = null;
var urlPage = null;


/**
 * get page from url, push html into iframe
 * @param {element} elm 
 */
function goToPage(elm) {
    urlPage = $('#inUrlPage').val();
    console.log(urlPage);
    let urlGetPage = addressServer + "/accessWebsite";

    // hide iframe
    $iFrame = $('#myiframe');
    $iFrame.hide();
    $loadingContainer = $('#loading-container');
    $loadingContainer.show();

    var hostName = getHostName(urlPage);
    axios.post(urlGetPage, {
        urlPage: urlPage
    })
        .then(response => {
            let iframe = document.getElementById("myiframe");
            let docIframe = new DOMParser().parseFromString(response.data, 'text/html');
            let iFrameHeader = docIframe.getElementsByTagName("head")[0];

            // add jquery
            let myscript = docIframe.createElement('script');
            myscript.type = 'text/javascript';
            myscript.src = "https://code.jquery.com/jquery-1.10.2.js";
            iFrameHeader.appendChild(myscript);


            // change src for img tag
            var imgs = docIframe.getElementsByTagName('img');
            changeAttributeTag(hostName, imgs, 'src');

            //change src for script tag
            var scriptTags = docIframe.getElementsByTagName('script');
            changeAttributeTag(hostName, scriptTags, 'src');

            //change src for link tag
            var linkTags = docIframe.getElementsByTagName('link');
            changeAttributeTag(hostName, linkTags, 'href');

            // add function javacript
            let selectorJS = docIframe.createElement('script');
            selectorJS.type = 'text/javascript';
            selectorJS.textContent = selectorJsContent;
            iFrameHeader.appendChild(selectorJS);


            $loadingContainer.fadeOut();
            $iFrame.show();

            // render iframe
            let docString = docIframe.getElementsByTagName("html")[0];
            iframe.srcdoc = docString.innerHTML;

            // setup listener from iFrame
            window.addEventListener('message', (event) => {
                let xpathElement = event.data;
                console.log('xPath Element:', xpathElement)
                let urlGetData = addressServer + '/getData';

                axios.post(urlGetData, {
                    typeData: typeXpath,
                    urlPage: urlPage,
                    xPathElm: xpathElement
                })
                    .then(response => {

                        if (typeXpath === 'table') {
                            // delete table
                            container.empty();
                            dataTable = response.data.table;
                            console.log('data table', dataTable);
                            let rowNum = dataTable.length;
                            let colNum = dataTable[0].length;
                            let currentTable = new Handsontable(container[0], {
                                data: dataTable,
                                minRows: rowNum,
                                minCols: colNum,
                                minSpareRows: 0,
                                colHeaders: false,
                                contextMenu: true
                            });
                            currentTable.render();
                        }
                        else {
                            let data = response.data;
                            elmFocus.val(data['data'])
                        }
                        typeXpath = '';

                    })
                    .catch(err => console.log(err));
            }, false);
        })
        .catch(err => console.log(err))
}


/**
 * get host name and protocol from url
 * @param {url of page} url 
 */
function getHostName(url) {
    let match = url.match(/.+:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 && typeof match[0] === 'string' && match[0].length > 0) {
        return match[0];
    } else {
        return null;
    }
}


/**
 * change host name attribute of tags
 * @param {host name} hostName 
 * @param {array tag} tags 
 * @param {attribute: herf, src,,,} attribute 
 */
function changeAttributeTag(hostName, tags, attribute) {
    for (let i = 0; i < tags.length; i++) {
        let src = tags[i].getAttribute(attribute);
        if (src == null) {
            continue;
        }
        // console.log(src);
        if (getHostName(src) == null) {
            if (src[0] == src[1] && src[0] == '/') {
                src = src.split('//')[1];
                // console.log(src);
                tags[i].setAttribute(attribute, 'https://' + src);
            }
            else {
                src[0] != '/' ? tags[i].setAttribute(attribute, hostName + '/' + src) : tags[i].setAttribute(attribute, hostName + src);
            }
            // tags[i].setAttribute('onerror', 'srcFail(this)');
        }
    }
}


(function ($) {
    $(function () {
        var addFormGroup = function (event) {
            event.preventDefault();

            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();

            $(this)
                .toggleClass('btn-primary btn-add btn-danger btn-remove')
                .html('–');

            $formGroupClone.find('.get-value').val('');
            $formGroupClone.find('.field-value').attr('hidden', false);
            $formGroupClone.find('.concept').text('Text');
            $formGroupClone.find('.data-table').empty();
            $formGroupClone.insertAfter($formGroup);

            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };

        var removeFormGroup = function (event) {
            event.preventDefault();

            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }

            $formGroup.remove();
        };

        var selectFormGroup = function (event) {
            event.preventDefault();

            var $selectGroup = $(this).closest('.input-group-select');
            var param = $(this).attr("href").replace("#", "");
            var concept = $(this).text();

            $selectGroup.find('.concept').text(concept);
            $selectGroup.find('.input-group-select-val').val(param);

        };

        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };

        var onFocus = function (event) {
            event.preventDefault();

            elmFocus = $(this);
            let $formGroup = $(this).closest('.form-group');
        }

        var showTable = function (event) {
            typeXpath = 'table';
            event.preventDefault();

            let $formGroup = $(this).closest('.form-group');
            $formGroup.find('.data-table').attr('hidden', false);

            // remove value and  hidden field value
            $formGroup.find('.field-name').val('')
            $formGroup.find('.field-value').val('');
            $formGroup.find('.field-value').attr('hidden', true);

            container = $formGroup.find('.data-table');
            // currentTable = new Handsontable(container, settingsTable);
            // currentTable.render();
        };

        var hiddenTable = function (event) {
            typeXpath = 'text';
            event.preventDefault();

            let $formGroup = $(this).closest('.form-group');

            $formGroup.find('.field-name').val('')
            $formGroup.find('.field-value').val('')
            $formGroup.find('.field-value').attr('hidden', false);

            $formGroup.find('.data-table').attr('hidden', true);

            container = null;
        }

        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
        $(document).on('click', '.dropdown-menu a', selectFormGroup);
        $(document).on('click', '.li-table ', showTable);
        $(document).on('click', '.li-text', hiddenTable);
        $(document).on('focus', '.input-value', onFocus)
    });
})(jQuery);
