const addressServer = "http://localhost:5000";

const selectorJsContent = `
    $(window).load(function(){
        
        function getPathTo(element) {
            if (element.id!=='')
                return "//*[@id='"+element.id+"']";
            
            if (element===document.body)
                return element.tagName.toLowerCase();

            var ix= 0;
            var siblings= element.parentNode.childNodes;
            for (var i= 0; i<siblings.length; i++) {
                var sibling= siblings[i];
                
                if (sibling===element) return getPathTo(element.parentNode) + '/' + element.tagName.toLowerCase() + '[' + (ix + 1) + ']';
                
                if (sibling.nodeType===1 && sibling.tagName === element.tagName) {
                    ix++;
                }
            }
        }

        $(document).on('click','*', function(ev){ 
            let xPath = getPathTo(ev.toElement);
            window.parent.postMessage(xPath, '*');
            return false;
        });
        
        $(document).mouseover(function(event) {
            if(event.target.tagName === 'BODY' || event.target.tagName === 'HTML' || event.target.tagName === 'MAIN' || event.target.tagName === 'IMG' || event.target.tagName === 'HEADER') return;
            let target = event.target;
            target.style.background = '#42adf4';
            target.style.transition = 'all 300ms ease';

        });

        $(document).mouseout(function(event) {
            if(event.target.tagName === 'BODY' || event.target.tagName === 'HTML') return;
            let target = event.target;
            target.style.background = '';
            target.style.transition = 'all 300ms ease';
        });
        
    });
    
    function srcFail(elm){
        console.log('hey, fail:', elm.getAttribute('src'));
    }
    
    `;


